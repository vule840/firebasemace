// import {firestoreAction} from 'vuexfire'
// import {dbMenuRef} from '../../firebase'
// import {dbOrdersRef} from '../../firebase'
import {firebaseAuth} from '../../firebase'


const state = {
    currentUser: null,
    currentUserRegister: null

}

const getters = {
  currentUser: state => state.currentUser
}

const mutations = {  
    userStatusRegister: (state, userRegister) => {

      if (userRegister === null) {
        state.currentUserRegister = null

      } else {
        console.log(currentUserRegister);
        state.currentUserRegister = currentUserRegister
      }
    },
    userStatus: (state, user) => {

      if (user === null) {
        state.currentUser = null
      } else {
        console.log(user);
        state.currentUser = user.email
      }
      // user === null ? state.currentUser = null : state.currentUser = user.email
    }
}

const actions = {

      registerIn: async ({ commit }, userRegister) => {
        try {
  
          const userRegisterData = await firebaseAuth
            .createUserWithEmailAndPassword(
              userRegister.registerEmail,
              userRegister.registerPassword
            )
          commit('userStatusRegister', userRegisterData.userRegister)
        } catch (error) {
          const errorCode = error.code
          const errorMessage = error.message
  
          if (errorCode === "auth/email-already-exists") {
            alert("Email already exists")
          } else {
            alert(errorMessage)
          }
  
          console.log(errorMessage);
        }
  
      },
      signIn: async ({ commit }, user) => {
        try {
          const userData = await firebaseAuth.signInWithEmailAndPassword(
            user.email,
            user.password
          )
          console.log(user);
          commit('userStatus', userData.user)
        } catch (error) {
  
  
          const errorCode = error.code
          const errorMessage = error.message
  
          switch (errorCode) {
            case "auth/email-already-exists":
              alert("This email already in use")
              break;
            case "auth/wrong-password":
              alert("Wrong password")
            case "auth/invalid-email":
              alert("Invalid email")
              case "auth/user-not-found":
                alert("user-not-found")
              
            default:
              break;
          }
          console.log(errorMessage);
        }
  
      },
      logOut: async ({ commit }, user) => {
        try {
          await firebaseAuth.signOut()
          commit('userStatus', null)
        } catch (error) {
          console.log(error);
        }
      }
}

export default {
    state,
    getters,
    mutations,
    actions

}