import { firestoreAction } from 'vuexfire'
import { dbCats } from '../../firebase'

const state = {
  cats: []
}

const getters = {
  getCats: state => state.cats
}

const mutations = {

}

const actions = {
  //vuex fire, importani vuexactions i vuexutations, dbCats
  setCatRef: firestoreAction(context => {
    return context.bindFirestoreRef('cats', dbCats.orderBy("ime","asc"))
  }),
  deleteCat: async ({ commit }, id) => {
    try {
      const cat = await dbCats.doc(id)
      cat.delete()
    } catch (error) {
      console.log(error);
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions

}