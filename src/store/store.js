import Vue from 'vue'
import Vuex from 'vuex'
// import { firebaseAuth } from '../firebase'
import {vuexfireMutations} from 'vuexfire'
import cats from './modules/cats'
import user from './modules/user'
// import {firestoreAction} from 'vuexfire'
// import {dbCats} from '../firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  mutations: vuexfireMutations,
  modules: {
    cats,
    user
  }
  // state: {
  //   currentUser: null,
  //   currentUserRegister: null,
  //   cats: []
  // },
  // getters: {
  //   // currentUser: state => state.currentUser
  //   getCats: state => state.cats
  // },

  // mutations: {
    
  //   userStatusRegister: (state, userRegister) => {

  //     if (userRegister === null) {
  //       state.currentUserRegister = null

  //     } else {
  //       console.log(currentUserRegister);
  //       state.currentUserRegister = currentUserRegister
  //     }
  //   },
  //   userStatus: (state, user) => {

  //     if (user === null) {
  //       state.currentUser = null
  //     } else {
  //       console.log(user);
  //       state.currentUser = user.email
  //     }
  //     // user === null ? state.currentUser = null : state.currentUser = user.email
  //   }

  // },
  // actions: {
  //   //vuex fire, importani vuexactions i vuexutations, dbCats
  //   setCatRef: firestoreAction(context => {
  //     return context.bindFirestoreRef('cats', dbCats)
  //   }),
  //   deleteCat: async ({commit}, id) => {
  //     try {
  //       const cat = await dbCats.doc(id)
  //       cat.delete()
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   },
  //   registerIn: async ({ commit }, userRegister) => {
  //     try {

  //       const userRegisterData = await firebaseAuth
  //         .createUserWithEmailAndPassword(
  //           userRegister.registerEmail,
  //           userRegister.registerPassword
  //         )
  //       commit('userStatusRegister', userRegisterData.userRegister)
  //     } catch (error) {
  //       const errorCode = error.code
  //       const errorMessage = error.message

  //       if (errorCode === "auth/email-already-exists") {
  //         alert("Email already exists")
  //       } else {
  //         alert(errorMessage)
  //       }

  //       console.log(errorMessage);
  //     }

  //   },
  //   signIn: async ({ commit }, user) => {
  //     try {
  //       const userData = await firebaseAuth.signInWithEmailAndPassword(
  //         user.email,
  //         user.password
  //       )
  //       console.log(user);
  //       commit('userStatus', userData.user)
  //     } catch (error) {


  //       const errorCode = error.code
  //       const errorMessage = error.message

  //       switch (errorCode) {
  //         case "auth/email-already-exists":
  //           alert("This email already in use")
  //           break;
  //         case "auth/wrong-password":
  //           alert("Wrong password")
  //         case "auth/invalid-email":
  //           alert("Invalid email")
  //           case "auth/user-not-found":
  //             alert("user-not-found")
            
  //         default:
  //           break;
  //       }
  //       console.log(errorMessage);
  //     }

  //   },
  //   logOut: async ({ commit }, user) => {
  //     try {
  //       await firebaseAuth.signOut()
  //       commit('userStatus', null)
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   }

  // },

})
