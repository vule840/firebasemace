    import firebase from 'firebase/app'
    import 'firebase/firestore'
    import 'firebase/auth'
    import 'firebase/storage'


    const config = {
        apiKey: "AIzaSyDTYLzJZegemM6E0KOGi5mVqH5ctf0CmRk",
        authDomain: "fir-test-f590e.firebaseapp.com",
        databaseURL: "https://fir-test-f590e.firebaseio.com",
        projectId: "fir-test-f590e",
        storageBucket: "fir-test-f590e.appspot.com",
        messagingSenderId: "939608961082",
        appId: "1:939608961082:web:1cc638bdad443c21c48503",
        clientId: "463711483324-b3bhjpg38j2khg3hmiq917hfmuvi08r6.apps.googleusercontent.com.apps.googleusercontent.com",
        scopes: [
            "email",
            "profile",
            "https://www.googleapis.com/auth/calendar"
   ]

    }
    
firebase.initializeApp(config)
export const db = firebase.firestore()
export const dbStorage = firebase.storage()
export const firebaseAuth = firebase.auth()
export const dbTest = db.collection('test')
export const dbCats = db.collection('cats')


